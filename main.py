from woker import Worker
import thread

def start_worker(worker):
	worker.run()
	pass

def start(num_worker):
	workers = []
	for i in range(num_worker):
		name ='worker%i' %(i)
		workers.append(Worker(name))
		thread.start_new_thread(start_worker, (workers[i],))
		print('%s is running' %(name))

	return workers


start(num_worker=8)

while 1:
	pass


