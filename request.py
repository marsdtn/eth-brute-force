import requests
import json;

class Request:
	@classmethod
	def get_balance(self, item, name=''):

		payload = {'jsonrpc': '2.0', 'method': 'eth_getBalance',
		           'params': [item.get_address(), 'latest'], 'id': 1}

		headers = {'Content-Type': 'application/json'}

		res = requests.post('http://127.0.0.1:8545', data=json.dumps(payload), headers=headers)

		balance = ((json.loads(res.content))['result']);

		print(name + ' : ' + item.get_address() + ', ' + res.content)

		return balance
