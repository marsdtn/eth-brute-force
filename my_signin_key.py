from hashlib import sha1
from ecdsa import SigningKey, SECP256k1
import sha3
import random
from item import Item

class MySigningKey(SigningKey):

	@classmethod
	def random(klass, curve=SECP256k1, hashfunc=sha1):

		n = curve.order - 1;

		secexp = random.randint(0, n)

		private_key = klass.from_secret_exponent(secexp, curve, hashfunc)

		public_key = private_key.get_verifying_key().to_string()

		keccak = sha3.keccak_256()

		keccak.update(public_key)
		address = '0x' +  keccak.hexdigest()[24:]

		item = Item(private_key=private_key.to_string().encode("hex"), address=address)

		return item